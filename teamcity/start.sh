#!/bin/bash
echo "Set docker images version"
export TEAMCITY_VERSION=2018.1.3-linux
export MYSQL_VERSION=10.3.10-bionic

echo "Generate root and teamcity user passwords"
export ROOT_PASSWORD=`openssl rand -base64 16`
export USER_PASSWORD=`openssl rand -base64 16`

echo "Run Teamcity server"
docker-compose -p teamcity up -d

echo "Coping MySQL jdbc connector"
sudo mkdir -p /var/lib/docker/volumes/teamcity_server_data/_data/lib/jdbc
sudo cp jdbc/mysql-connector-java-8.0.12.jar /var/lib/docker/volumes/teamcity_server_data/_data/lib/jdbc

echo "MySQL root password: $ROOT_PASSWORD"
echo "MySQL teamcity user password: $USER_PASSWORD"

echo "Clean used variables"
unset ROOT_PASSWORD
unset USER_PASSWORD
unset TEAMCITY_VERSION
unset MYSQL_VERSION
